<?php
declare(strict_types=1);

namespace Soong\Console\Tests\Command;

use Soong\KeyMap\KeyMapBase;

/**
 * Simple key map implementation using serialize().
 */
class SerializeKeyMap extends KeyMapBase
{

    use SerializeTrait;

    /**
     * @inheritdoc
     */
    protected function optionDefinitions(): array
    {
        $options = parent::optionDefinitions();
        $options['file'] = [
            'required' => true,
            'allowed_types' => 'string',
        ];
        return $options;
    }

    /**
     * @inheritdoc
     */
    public function saveKeyMap(array $extractedKey, array $loadedKey) : void
    {
        $extractedKey = array_values($extractedKey);
        $loadedKey = array_values($loadedKey);
        $keyMap = $this->getData();
        $keyMap[$this->hashKeys($extractedKey)] = [$extractedKey, $loadedKey];
        $this->putData($keyMap);
    }

    /**
     * @inheritdoc
     */
    public function lookupLoadedKey(array $extractedKey) : array
    {
        // Loaded key array is the second element of the key map entry.
        return $this->getData()[$this->hashKeys($extractedKey)][1];
    }

    /**
     * @inheritdoc
     */
    public function lookupExtractedKeys(array $loadedKey) : array
    {
        foreach ($this->getData() as $keyMapEntry) {
            [$mapExtractedKey, $mapLoadedKey] = $keyMapEntry;
            if ($loadedKey == $mapLoadedKey) {
                return $mapExtractedKey;
            }
        }
        return [];
    }

    /**
     * @inheritdoc
     */
    public function delete(array $extractedKey) : void
    {
        $keyMap = $this->getData();
        unset($keyMap[$this->hashKeys($extractedKey)]);
        $this->putData($keyMap);
    }

    /**
     * @inheritdoc
     */
    public function count() : int
    {
        return count($this->getData());
    }

    /**
     * @inheritdoc
     */
    public function iterate() : iterable
    {
        foreach ($this->getData() as $keyValues) {
            // Extracted keys are the first array member.
            yield $keyValues[0];
        }
    }
}
