<?php
declare(strict_types=1);

namespace Soong\Console\Tests\Command;

trait SerializeTrait
{

    /**
     * Serialize the data and save to the configured file.
     *
     * @param mixed $data
     */
    protected function putData($data) : void
    {
        file_put_contents($this->getConfigurationValue('file'), serialize($data));
    }

    /**
     * Retrieve the data from the configured file.
     *
     * @return array
     */
    protected function getData() : array
    {
        if ($dataSerialized = @file_get_contents($this->getConfigurationValue('file'))) {
            return unserialize($dataSerialized);
        } else {
            return [];
        }
    }
}
